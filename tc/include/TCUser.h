//
// Created by Jean-Julien ZEIL on 25/02/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCMacros.h"


@interface TCUser : NSObject

SINGLETON_CLASS_H(TCUser)

- (void) setLongitude: (float) longitude  andLatitude: (float) latitude;
- (void) addUserCookieByKey: (NSString *) key andValue: (NSString *) value;
- (void) addKeywordByKey: (NSString *) key andValue: (NSString *) value;

@property (nonatomic, assign) float longitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) float horizontalAccuracy;
@property (nonatomic, assign) float verticalAccuracy;

@property (nonatomic, retain) NSString *userID;
@property (nonatomic, retain) NSMutableDictionary *userCookies;
@property (nonatomic, retain) NSMutableDictionary *keywords;
@property (nonatomic, assign) int age;
@property (nonatomic, retain) NSString *gender;

@end
