//
//  TCContainer.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCTag.h"

@class TCSampling;
@class TCFunction;

@interface TCContainer : NSObject <ITCDynamicStore>

- (NSArray *) tagsWithLibID: (NSString *) libID withType: (enum TCTagEventType) searchedType;
- (void) addTagToContainer: (TCTag *) tag;
- (void) addFunctionToContainer: (TCFunction *) function;
- (TCFunction *) getFunctionByName: (NSString *) name;

@property (nonatomic, assign) double frequency;
@property (nonatomic, assign) NSString *ip;
@property (nonatomic, retain) NSMutableArray *tags;
@property (nonatomic, retain) NSMutableArray *functions;
@property (nonatomic, retain) NSMutableSet *libIDs;
@property (nonatomic, retain) TCDynamicStore *dynamicStore;
@property (nonatomic, retain) TCSampling *sampling;

@end
