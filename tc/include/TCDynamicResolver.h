//
//  TCDynamicResolver.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCTag.h"
#import "ITCDynamicStore.h"
#import "TCConstants.h"

@class TCTag;


@interface TCDynamicResolver : NSObject

- (void) addStore: (id <ITCDynamicStore>) store;
- (void) insertStore: (id <ITCDynamicStore>) store atIndex: (int) index;
- (BOOL) isResolvable: (NSString *) variableNameToResolve;
- (BOOL) isKnown: (NSString *) variableNameToResolve;
- (NSString *) getResolvedValue: (NSString *) variableNameToResolve;

- (NSString *) replaceKnownValuesInString: (NSString *) toReplace;
- (NSString *) replaceKnownValues: (TCShadowValue *) toReplace;
- (TCShadowValue *) replaceKnownShadowValues: (TCShadowValue *) toReplace;

- (NSString *) valueInternalVariables: (TCShadowValue *) shadowValue withDynamicStore: (TCDynamicStore *) store;

- (NSString *) get: (NSString *) variableName;
- (void) solveDynamicVariablesInTag: (TCTag *) tag;
- (void) solveTagParameters: (TCTag *) tag;
+ (void) emptyNonExistingParametersInTag: (TCTag *) tag;

@property(nonatomic, retain) NSMutableArray *storeCollection; /**< TCDynamicStore storage */

@end
