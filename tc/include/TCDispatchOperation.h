//
// Created by Jean-Julien ZEIL on 26/02/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCHit.h"


@interface TCDispatchOperation : NSOperation

- (id) initWithTCHit: (TCHit *) hit;

@property (nonatomic, retain) TCHit *hit;

@end