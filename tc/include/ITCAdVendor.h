//
// Created by Jean-Julien ZEIL on 11/02/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIView;
@class TCTag;
@class TCAdOperation;

@protocol ITCAdVendor <NSObject>

- (TCAdOperation *) createOperationForTag: (TCTag *) tag view: (UIView *) adView;

@end
