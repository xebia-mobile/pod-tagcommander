//
// Created by Jean-Julien ZEIL on 12/03/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCAppVars;
@class TagCommander;

@interface TCAppVarsOperation : NSOperation

- (id) initWithAppVars: (TCAppVars *) appVars forTagCommander: (TagCommander *) TC;

@property (nonatomic, retain) TCAppVars *appVars;
@property (nonatomic, retain) TagCommander *TC;

@end