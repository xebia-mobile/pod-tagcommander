//
//  ITCWaitingQueueDelegate.h
//  TagCommander
//
//  Created by Damien Terrier on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCHit.h"

/**
* @protocol ITCWaitingQueueDelegate
*
* Protocol to have a waiting queue
*
* @see TCWaitingQueue
*
*/
@protocol ITCWaitingQueueDelegate

/**
* Compiles the conditions to process the Waiting Condition
*
* @param hit Compile Required Conditions
*/
- (void) compileRequiredConditions: (TCHit *) hit;

/**
* Process Valid Hit
*
* @param hit Processed hit
*/
- (void) processValidHit: (TCHit *) hit;

@end
