//
// Created by Damien TERRIER on 05/08/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef CRITEO_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "TCVendor.h"

@interface TCCriteo_10 : TCVendor <ITCVendor>

SINGLETON_CLASS_H(TCCriteo_10)

@end

#endif
