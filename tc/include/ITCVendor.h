//
// Created by Damien TERRIER on 19/09/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//


@class TCHit;
#ifndef __ITCVendor_H_
#define __ITCVendor_H_

/**
* @protocol ITCVendor
*
* Every child vendor instance should conforms to the ITCVendor protocol.
* This protocol defines what method should be available to be called later.
*
* The required one is onHitExecute.
*
* onHitExecute is the notification when the TCVendor singleton
* received a new hit execute notification.
* @warning : The TCVendor singleton receives **all the vendor notifications**
* even the ones not for the vendor. The first thing is to filter to keep the ones for this vendor
* by looking at the libID.
*
* Optional notifications:
* - onVendorInit is the notification when the TCParser has found an init tag.
* - onVendorConfiguration has to be called by the client but is the generic one for additional configuration of a vendor.
* It's optional because not all the TCVendor singleton have a need for data at initialisation or configuration.
*
*/
@protocol ITCVendor <NSObject>
- (BOOL) onHitExecute: (TCHit *) hit;

@optional
- (BOOL) onVendorInit: (NSNotification *) incomingInitNotification;
- (BOOL) onVendorConfiguration: (NSNotification *) incomingConfigurationNotification;

@end

#endif //__ITCVendor_H_
