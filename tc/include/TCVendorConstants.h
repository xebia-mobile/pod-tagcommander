//
// Created by Damien TERRIER on 05/08/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - vendors IDs

extern NSString *const kTCVendorLibID_allVendors;

extern NSString *const kTCVendorLibID_SmartAdServer_441;
extern NSString *const kTCVendorClassName_SmartAdServer_441;

extern NSString *const kTCVendorLibID_Flurry_530;
extern NSString *const kTCVendorClassName_Flurry_530;

extern NSString *const kTCVendorLibID_Flurry_620;
extern NSString *const kTCVendorClassName_Flurry_620;

extern NSString *const kTCVendorLibID_Flurry_711;
extern NSString *const kTCVendorClassName_Flurry_711;

extern NSString *const kTCVendorLibID_customURL;
extern NSString *const kTCVendorClassName_customURL;

extern NSString *const kTCVendorGoogleAnalyticsHitRegexp;
extern NSString *const kTCVendorCriteoHitRegexp;
extern NSString *const kTCVendorComScoreHitRegexp;

extern NSString *const kTCVendorLibID_googleConversion_311;
extern NSString *const kTCVendorClassName_googleConversion_311;

extern NSString *const kTCVendorLibID_googleAnalytics_5077;
extern NSString *const kTCVendorClassName_googleAnalytics_5077;

extern NSString *const kTCVendorLibID_criteo_10;
extern NSString *const kTCVendorClassName_criteo_10;

extern NSString *const kTCVendorLibID_comScore_21409;
extern NSString *const kTCVendorClassName_comScore_21409;

extern NSString *const kTCVendorLibID_AdMaster_120;
extern NSString *const kTCVendorClassName_AdMaster_120;

extern NSString *const kTCVendorLibID_unknownVendor;

extern NSString *const kTCVendorInstanceMethod;

extern NSString *const kTCVendorFunction;
extern NSString *const kTCVendorConfiguration;

extern NSString *const kTCVendorFunction;
extern NSString *const kTCVendorInit;
extern NSString *const kTCVendorKey;
extern NSString *const kTCVendorURL;
extern NSString *const kTCAdTimeout;
