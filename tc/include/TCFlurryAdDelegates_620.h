//
// Created by Jean-Julien ZEIL on 12/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef FLURRY_620_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "FlurryAdBannerDelegate.h"
#import "FlurryAdInterstitialDelegate.h"

@interface TCFlurryAdDelegates_620 : NSObject<FlurryAdBannerDelegate, FlurryAdInterstitialDelegate>

SINGLETON_CLASS_H(TCFlurryAdDelegates_620)

+ (void) registerBannerListener: (id<FlurryAdBannerDelegate>) listener;
+ (void) registerInterstitialListener: (id<FlurryAdInterstitialDelegate>) listener;

@property (nonatomic, retain) id<FlurryAdBannerDelegate> bannerListener;
@property (nonatomic, retain) id<FlurryAdInterstitialDelegate> interstitialListener;

@end

#endif