//
// Created by Damien TERRIER on 05/08/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef GOOGLE_CONVERSION_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "TCVendor.h"

@interface TCGoogleConversion_311 : TCVendor <ITCVendor>

SINGLETON_CLASS_H(TCGoogleConversion_311)

@property (nonatomic) NSString *conversionID;
@property (nonatomic) NSString *label;
@property (nonatomic) NSString *value;
@property (nonatomic) BOOL isRepeatable;

@end

#endif
