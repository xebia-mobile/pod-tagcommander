#import "TagCommander.h"

@class NSOperationQueue;
@class NSOperationQueue;
@class TCSampling;
@class TCContainerFactory;
@class TCInitialisation;
@class TCAdServing;
@class TCDynamicResolver;


@interface TagCommander ()
- (TCHit *) createHitWithAppVars: (TCAppVars *) appVars tag: (TCTag *) tag;
- (void) saveAllHits;
- (TCContainer *) containerWithData: (NSData *) xmlData withResolver: (TCDynamicResolver *) dynamicResolver;

- (void) restoreStockedHit;

/**
* Unit testing init.
*
*/
- (id) initWithSiteID: (int) siteID andContainerID: (int) containerID andNotificationCenter: (NSNotificationCenter *) notificationCenter;

@property (nonatomic, retain) NSOperationQueue *queue;
@property (nonatomic, retain) TCSampling *sampling;
@property (nonatomic, retain) TCContainerFactory *containerFactory;
@property (nonatomic, strong) TCInitialisation *initialisation;

@end
