//
// Created by Jean-Julien ZEIL on 06/03/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCEventSender.h"

@class TCContainer;
@class TagCommander;
@class TCContainerConfiguration;


static NSString *const kTCLastDownloadTime = @"ContainerLastDownloadTime_%i_%i";

@interface TCContainerFactory : NSObject<ITCEventSenderDelegate>

- (id) initWithSiteID: (int) siteID andContainerID: (int) containerID;

- (void) containerForConfigurationString: (NSString *) configurationString withTagCommander: (TagCommander *) TC;
- (TCContainer *) defaultContainerForFileName: (NSString *) fileName andBundle: (NSBundle *) bundle;
- (TCContainer *) defaultContainer;

@property (nonatomic, assign) int siteID;
@property (nonatomic, assign) int containerID;
@property (nonatomic, retain) TCContainerConfiguration *configuration;
@property (nonatomic, assign) BOOL isFetchingConfiguration;

@property (nonatomic, retain) TCEventSender *senderDelegate;

@end