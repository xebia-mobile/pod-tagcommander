//
// Created by Damien TERRIER on 17/09/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef SMART_AD_SERVER_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "TCVendor.h"
#import "TCAdVendor.h"
#import "ITCAdVendor.h"

@class UIView;
@class TCAdOperation;

@interface TCSmartAdServer_441 : TCAdVendor<ITCVendor, ITCAdVendor>

SINGLETON_CLASS_H(TCSmartAdServer_441)

- (TCAdOperation *) createOperationForTag: (TCTag *) tag view: (UIView *) adView;

@end

#endif