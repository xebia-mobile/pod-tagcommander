//
//  TCFlurry_711+FlurryCompatibility.h
//  TagCommander
//
//  Created by Damien TERRIER on 20151009.
//  Copyright © 2015 TagCommander. All rights reserved.
//


@interface TCFlurry_711 (FlurryCompatibility)

+ (BOOL) activeSessionExists;
+ (void) setSessionContinueSeconds:(int)seconds;
+ (void) setCrashReportingEnabled:(BOOL)value;
+ (void) addSessionOrigin:(NSString *)sessionOriginName  withDeepLink:(NSString*)deepLink;
+ (void) addSessionOrigin:(NSString *)sessionOriginName;
+ (void) sessionProperties:(NSDictionary *)parameters;
+ (void) addOrigin:(NSString *)originName withVersion:(NSString*)originVersion;
+ (void) addOrigin:(NSString *)originName withVersion:(NSString*)originVersion withParameters:(NSDictionary *)parameters;
+ (void) logError:(NSString *)errorID message:(NSString *)message error:(NSError *)error;
+ (void) logAllPageViewsForTarget:(id)target;
+ (void) stopLogPageViewsForTarget:(id)target;
+ (void) logPageView;
+ (NSString*) getSessionID;
+ (void) setUserID:(NSString *)userID;
+ (void) setAge:(int)age;
+ (void) setGender:(NSString *)gender;
+ (void) setLatitude:(double)latitude
           longitude:(double)longitude
  horizontalAccuracy:(float)horizontalAccuracy
    verticalAccuracy:(float)verticalAccuracy;
+ (void) setPulseEnabled: (BOOL) enable_pulse;

@end
