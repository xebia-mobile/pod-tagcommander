//
//  TCNetworkManager.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCMacros.h"

#import "ITCEventListenerDelegate.h"
#import "TCEventListener.h"

#import "ITCEventSenderDelegate.h"
#import "TCEventSender.h"

#import "ITCWaitingQueueDelegate.h"
#import "TCSingleton.h"

@interface TCNetworkManager : TCSingleton <ITCEventListenerDelegate, ITCEventSenderDelegate>

typedef enum TCRequestTypes
{
    TCUnknownRequestType, /** an unknown network request. Does nothing */
    TCConfigurationRequestType, /** A configuration file request. TagCommander internals */
    TCHTTPRequestType, /** An HTTP GET Request */
    TCImageRequestType /** not used, maybe for ad serving download */
} TCRequestTypes;


+ (BOOL) isValidURL: (NSString *) url;
+ (NSString *) notificationResponseNameForRequestType: (NSNumber *) requestType;
+ (NSString *) notificationRequestNameForRequestType: (TCRequestTypes) requestType;
+ (NSNumber *) requestTypeForNotificationName: (NSString *) notificationName;

SINGLETON_CLASS_H(TCNetworkManager)

- (id) reinit;
- (void) postNetworkNotification: (NSString *) notificationName withUserInfo: (NSDictionary *) userInfo;
- (void) changeOfflineConfigurationsForMaxLifetime: (int) lifetime andMaxOperationsInQueue: (int) maxRequestsInQueue;

@property(nonatomic, retain) TCEventSender *senderDelegate;
@property(nonatomic, retain) TCEventListener *listenerDelegate;
@property(strong) NSOperationQueue *operationQueue;

@property(nonatomic, assign, readonly) unsigned int nextRequestID;
@property(nonatomic, assign, readonly) unsigned int totalRequestSent;
@property(nonatomic, assign, readonly) unsigned int maxRequestLifetime;
@property(nonatomic, assign, readonly) unsigned int maxRequestsInQueue;

@end
