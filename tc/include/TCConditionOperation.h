//
// Created by Jean-Julien ZEIL on 24/02/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCConditionOperation : NSObject

- (id) initWithTestAgainst: (NSString *) test andPossibleValues: (NSMutableArray *) values;

@property(nonatomic, retain) NSString *testAgainst;
@property(nonatomic, retain) NSMutableArray *possibleValues;

@end