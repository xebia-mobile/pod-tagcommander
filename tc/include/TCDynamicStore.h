//
//  TCDynamicStore.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ITCDynamicStore.h"

@class TCAdditionalParameters;

@interface TCDynamicStore : NSObject<ITCDynamicStore, NSCoding>

- (NSArray *) recoverMapList;
- (NSArray *) recoverMapListWithKey: (BOOL) withKey;
- (void) copyDynamicStore: (TCDynamicStore *) store;

@property (nonatomic, retain) NSMutableDictionary *dynamicStore;
@property (nonatomic, retain) NSMutableArray *orderedKeys;
@property (nonatomic, retain) TCAdditionalParameters *additionalParameters;

@end
