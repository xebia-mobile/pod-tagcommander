//
// Created by Jean-Julien ZEIL on 23/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef SMART_AD_SERVER_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "SASAdViewDelegate.h"

@interface TCSmartAdServerDelegates_441 : NSObject<SASAdViewDelegate>

SINGLETON_CLASS_H(TCSmartAdServerDelegates_441)

- (void) registerListener: (id<SASAdViewDelegate>) listener;

@property (nonatomic, retain) id<SASAdViewDelegate> listener;

@end

#endif