//
// Created by Jean-Julien ZEIL on 11/08/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCDynamicStore;

typedef enum TCFunctionTypes
{
    TCFunctionTypes_Replace,
    TCFunctionTypes_Mapped,
    TCFunctionTypes_Split,
    TCFunctionTypes_Concat,
    TCFunctionTypes_Unknown,
} ETCFunctionTypes;

@interface TCFunction : NSObject

- (id) initWithName: (NSString *) name type: (ETCFunctionTypes) type andArguments: (NSArray *) arguments;

- (NSString *) apply: (NSString *) in;
- (NSString *) apply: (NSString *) in withDynamicStore: (TCDynamicStore *) store;

+ (ETCFunctionTypes) functionTypeFromString: (NSString *) type;

@property (nonatomic, retain) NSString *name;
@property (nonatomic, assign) ETCFunctionTypes type;
@property (nonatomic, retain) NSArray *arguments;

@property (nonatomic, assign) BOOL isValid;

@end