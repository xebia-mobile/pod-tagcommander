//
// Created by Damien TERRIER on 150316.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ITCCustomHTTPRequest <NSObject>

+ (NSMutableURLRequest *) customizeRequest: (NSMutableURLRequest *) URLRequest;

@end
