//
//  TCFlurryVendor_711.h
//  TagCommander
//
//  Created by Damien TERRIER on 20151009.
//  Copyright © 2015 TagCommander. All rights reserved.
//


#ifdef FLURRY_ENABLED


#import <Foundation/Foundation.h>

#import "TCMacros.h"
#import "TCVendor.h"


@interface TCFlurry_711 : TCVendor<ITCVendor>
SINGLETON_CLASS_H(TCFlurry_711)


- (void) userAnalyticsAdapter;

@end

#endif

