//
//  TCInitialisation.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCContainer;
@class TagCommander;
@class TCVendor;

@interface TCInitialisation : NSObject

- (id) initWithTagCommander: (TagCommander *) tagCommander;
- (void) initVendors: (TCContainer *) container;

- (void) sendVendorsInit: (TCContainer *) container;

- (void) sendVendorCreated: (NSString *) vendorClassName;
- (void) sendVendorError: (NSString *) vendorClassName;
- (void) sendVendorFound: (NSString *) vendorClassName;
- (void) sendVendorNotification: (NSString *) notificationName withVendorClass: (NSString *) vendorClassName;

- (TCVendor *) instantiateVendorFromClassName: (NSString *) vendorClassName;
@property (nonatomic, retain) TagCommander *tagCommander;
@end
