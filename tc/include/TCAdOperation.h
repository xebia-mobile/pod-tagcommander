//
// Created by Jean-Julien ZEIL on 09/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class TCTag;
@class UIView;

@interface TCAdOperation : NSObject

- (BOOL) callCleanOperationSelector;
- (void) initializeTimeout: (NSString *) timeout;
- (void) cleanOperation;

@property (nonatomic, retain) TCTag* tag;
@property (nonatomic, assign) UIView *adView;
@property (nonatomic, retain) UIViewController *adViewController;
@property (nonatomic, retain) NSString *adID;
@property (nonatomic, assign) BOOL operationCompleted;

@property (nonatomic, assign) double timestamp;
@property (nonatomic, assign) double timeout;

@end