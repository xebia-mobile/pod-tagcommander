//
//  TCCheckCondition.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCEventListener.h"

@class TCContainer;
@class TCTag;
@class TCDynamicResolver;
@class TCAppVars;
@class TagCommander;

@interface TCCheckCondition : NSObject

- (NSArray *) findTagsInContainer: (TCContainer *) container withAppVars: (TCAppVars *) appVars andTagCommander: (TagCommander *) TC;
- (BOOL) checkTag: (TCTag *)tag withSolver: (TCDynamicResolver *)solver;

@end
