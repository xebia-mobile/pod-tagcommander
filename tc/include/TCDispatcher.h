//
//  TCDispatcher.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCConstants.h"
#import "TCMacros.h"

#import "ITCEventListenerDelegate.h"
#import "ITCEventSenderDelegate.h"
#import "ITCWaitingQueueDelegate.h"

@class TCHit;
@class TCEventSender;
@class TCEventListener;
@class TCSampling;

@interface TCDispatcher : NSObject <ITCEventListenerDelegate, ITCEventSenderDelegate, ITCWaitingQueueDelegate>

SINGLETON_CLASS_H(TCDispatcher)

- (id) reinit;
- (void) dispatchHitToOperationQueue: (TCHit *) hit;
- (void) dispatchDependencyWithName: (NSString *) name;

- (void) replaceWaitingParametersForDependency: (NSString *) name;

- (void) processValidHit: (TCHit *) hit;

+ (NSDictionary *) packData: (id) anyData;
+ (id) unpackData: (NSDictionary *) userInfo;

- (void) changeOfflineConfigurationsForMaxLifetime: (int) lifetime andMaxOperationsInQueue: (int) operations;

- (void) restoreStoredHit;
+ (NSArray *) deserializeStoredHitsAtPath: (NSString *) path;
- (void) saveOfflineHitsToPath: (NSString *) offlineHitsPath;
- (void) removeOperationsInQueue;

@property (nonatomic, retain) TCEventSender *senderDelegate;
@property (nonatomic, retain) TCEventListener *listenerDelegate;

@property (nonatomic, assign, readonly) int nextHitID;
@property (nonatomic, assign, readonly) int totalHitSent;
@property (nonatomic, assign, readonly) int offlineHitMaxLifeTime;
/** Get the max time we keep an offline hit stored. */
@property (nonatomic, assign, readonly) int offlineHitMaxHitInQueue;
/** the maximum number of operations that can be simultaneously stored in the operation queue. */

@property (nonatomic, retain) NSOperationQueue *queue;
@property (nonatomic, retain) NSMutableDictionary *dependencies;
@property (nonatomic, retain) NSMutableDictionary *conditionsList;

@property (nonatomic, retain) NSMutableSet *knownVendors;
@end
	