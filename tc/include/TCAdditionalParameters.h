//
// Created by Jean-Julien ZEIL on 14/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCAdditionalParameters : NSObject

- (NSMutableDictionary *) get: (NSString *) type;
- (NSMutableDictionary *) getForVendor: (NSString *) vendor;

- (void) putForVendor: (NSString *) vendorID parameter: (NSString *) parameter withValue: (NSString *) value;
- (void) putParametersForAllVendors: (NSString *) parameter withValue: (NSString *) value;

@property (nonatomic, retain) NSMutableDictionary *parameters;

@end
