//
// Created by Jean-Julien ZEIL on 10/02/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCVendor.h"

@class TCAdOperation;
@class UIView;

@interface TCAdVendor : TCVendor
{
    NSMutableArray *_operationRunning;
}

- (void) removeAd: (NSString *) adID;
- (void) cleanOperationRunningList;

@property (nonatomic, retain) NSMutableArray *operationRunning;

@end
