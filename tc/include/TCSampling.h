//
// Created by Damien Terrier on 25/03/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCSampling : NSObject

@property(nonatomic, assign) int siteID;
@property(nonatomic, assign) int containerID;
@property(nonatomic, retain) NSString *containerVersion;
@property(nonatomic, assign, setter=setFrequency:) int frequency;
@property(nonatomic, retain) NSString *sampleURL;

@property(nonatomic, strong) NSNotificationCenter *notificationCenter;
@property(nonatomic, setter=setSeed:) unsigned int seed;
- (NSString *) formatURL;
- (id) init;
- (BOOL) sample;
@end
