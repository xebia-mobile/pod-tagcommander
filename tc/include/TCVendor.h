//
//  TCVendor.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCVendorConstants.h"
#import "ITCDynamicStore.h"
#import "ITCEventSenderDelegate.h"
#import "ITCEventListenerDelegate.h"
#import "ITCVendor.h"

@class TCDynamicStore;
@class TCEventListener;
@class TCEventSender;
@class TCHit;
@class TCVendorOperation;
@class TCTag;

@interface TCVendor : NSObject <ITCEventSenderDelegate, ITCEventListenerDelegate>
{
    NSString *_libID;
    TCEventListener *_listenerDelegate;
}

- (BOOL) callOnHitExecuteSelector: (TCHit *) hit;
- (BOOL) shouldWeTreatHit: (NSNotification *) notification;

- (void) sendHitSentNotification: (NSDictionary *) userInfo;
- (void) sendHitErrorNotification: (NSDictionary *) userInfo;

+ (NSString *) classNameFromLibID: (NSString *) libID;
+ (BOOL) areAllVariables: (NSArray *) variables fromStore: (id <ITCDynamicStore>) dynamicStore withVendorFunction: (NSString *) vendorFunction;

+ (NSString *) vendorIDFromURL: (NSString *) regexp;

@property (nonatomic, copy) NSString *libID;
@property (nonatomic, copy) NSString *className;

@property (nonatomic, retain) TCDynamicStore *dynamicStore;
@property (nonatomic, retain) TCEventSender *senderDelegate;
@property (nonatomic, retain) TCEventListener *listenerDelegate;
@property (nonatomic, retain) NSOperationQueue *queue;

@property (nonatomic, assign) int sentHits;
@property (nonatomic, assign) BOOL isInitialized;

@end
