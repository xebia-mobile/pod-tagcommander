//
//  TCTag.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ITCDynamicStore.h"

@class TCCondition;
@class TCDynamicStore;

typedef enum TCTagEventType
{
    TCTagEventType_Init,
    TCTagEventType_Execute,
    TCTagEventType_CustomURL,
    TCTagEventType_Unknown
} TCTagEventType;

typedef enum TCTagType
{
    TCTagType_Analytics,
    TCTagType_Ad,
    TCTagType_Unknown
} TCTagType;

@class TCCondition;

@interface TCTag : NSObject <ITCDynamicStore, NSCoding, NSCopying>
- (NSString *) shortDescription;
+ (BOOL) isDataTag: (id) userInfoData;
+ (BOOL) isInitVendorTag: (TCTag *) tag;

@property (nonatomic, retain) NSString *ID;
@property (nonatomic, retain) NSString *libID;
@property(nonatomic, retain) NSString *URL;
@property (nonatomic, assign) enum TCTagType type;
@property (nonatomic, assign) enum TCTagEventType eventType;

@property (nonatomic, retain) TCCondition *condition;
@property (nonatomic, retain) TCDynamicStore *dynamicStore;


@end


