//
// Created by Jean-Julien ZEIL on 11/08/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TCShadowValue : NSObject<NSCoding>

- (id) initWithValue: (NSString *) value;
- (id) initWithShadowValue: (TCShadowValue *) shadowValue;
- (id) initWithValue: (NSString *) value andFunctions: (NSArray *) function;
- (NSString *) description;

@property (nonatomic, retain) NSString *variableValue;
@property (nonatomic, retain) NSMutableArray *variableFunctions;

@end