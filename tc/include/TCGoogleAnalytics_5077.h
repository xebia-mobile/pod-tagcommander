//
// Created by Damien TERRIER on 05/08/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef GOOGLE_ANALYTICS_ENABLED

#import <Foundation/Foundation.h>

#import "TCMacros.h"
#import "TCVendor.h"

#import "TCCustomURL_210.h"
#import "ITCCustomHTTPRequest.h"


@interface TCGoogleAnalytics_5077 : TCCustomURL_210 <ITCVendor, ITCCustomHTTPRequest>

SINGLETON_CLASS_H(TCGoogleAnalytics_5077)

- (void) googleProduct: (TCHit *) hit;

@end

#endif
