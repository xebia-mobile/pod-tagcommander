//
//  TCRule.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCDynamicResolver.h"
#import "TCConditionOperation.h"

typedef enum TCRuleTypes
{
    TCRuleType_Equal,
    TCRuleType_NotEqual,
    TCRuleType_Or,
    TCRuleType_Nand,
    TCRuleType_OrMultiple,
    TCRuleType_AndMultiple,
    TCRuleType_Contains,
    TCRuleType_Exists,
    TCRuleType_GreaterThan,
    TCRuleType_LessThan,
    TCRuleType_IsEmpty,
    TCRuleType_Sampling,
    TCRuleType_Unknown
} TCRuleTypes;

@class TCDynamicResolver;

@interface TCRule : NSObject

- (id) initWithType: (TCRuleTypes) type;

- (void) addOperation: (TCConditionOperation *) operation;

- (TCRule *) copyTCRule;

- (BOOL) match: (TCDynamicResolver *) dynSolver;

- (BOOL) checkEqualCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkNotEqualCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkOrCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkNandCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkOrMultipleCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkAndMultipleCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkContainsCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkExistsCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkGreaterThanCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkLessThanCondition: (TCDynamicResolver *) dynSolver;

- (BOOL) checkIsEmptyCondition: (TCDynamicResolver *) dynSolver;

@property (nonatomic, assign) enum TCRuleTypes type;
@property (nonatomic, retain) NSMutableArray *operations;

@end
