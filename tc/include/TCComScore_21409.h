//
// Created by Jean-Julien ZEIL on 21/10/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef COMSCORE_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "TCVendor.h"

static NSString *const kTCCrossPublisherID = @"#TC_CROSSPUBLISHERID#";
static NSString *const kTCADID = @"#TC_ADID#";
static NSString *const kTCHitSent = @"#TC_HITS_SENT#";
static NSString *const kTCClient = @"#CLIENT#";
static NSString *const kTCClientID = @"clientID";
static NSString *const kTCSiteClient = @"#SITE_CLIENT#";
static NSString *const kTCSiteID = @"siteID";
static NSString *const kTCPublisherSecret = @"publisherSecret";
static NSString *const kTCCustomerC2 = @"#CUSTOMER_C2#";
static NSString *const kTCCustomerC2Name = @"customerC2";
static NSString *const kTCNSSite = @"#NS_SITE#";
static NSString *const kTCNSSiteName = @"ns_site";

@interface TCComScore_21409 : TCVendor <ITCVendor>

SINGLETON_CLASS_H(TCComScore_21409)

- (NSString *) computeVisitorIDMD5: (NSString *) publisherSecret;
- (NSString *) computeNS_AK;

- (NSString *) MD5: (NSString *) convert;

+ (void) setPublisherSecret: (NSString *) publisherSecret ClientID: (NSString *) client AndSiteID: (NSString *) site;
+ (void) setPublisherSecret: (NSString *) publisherSecret customerC2: (NSString *) c2 andNSSite: (NSString *) site;

+ (void) onUXActive;
+ (void) onUXInactive;

@property (nonatomic, retain) NSString *baseURL;
@property (nonatomic, retain) NSString *publisherSecret;
@property (nonatomic, retain) NSString *clientID;
@property (nonatomic, retain) NSString *siteID;
@property (nonatomic, retain) NSString *customerC2;
@property (nonatomic, retain) NSString *ns_site;
@property (nonatomic, retain) NSString *crossPublisherID;
@property (nonatomic, retain) NSString *ADID;

@property (nonatomic, assign) unsigned long long uxBackgroundTime;
@property (nonatomic, assign) unsigned long long uxDelta;
@property (nonatomic, assign) unsigned long long uxStartTime;

@property (nonatomic, assign) BOOL vendorStarted;

@end

#endif
