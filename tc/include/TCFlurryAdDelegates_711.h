//
// Created by Jean-Julien ZEIL on 12/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef FLURRY_ADSERVING_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "FlurryAdBannerDelegate.h"
#import "FlurryAdInterstitialDelegate.h"

@interface TCFlurryAdDelegates_711 : NSObject<FlurryAdBannerDelegate, FlurryAdInterstitialDelegate>

SINGLETON_CLASS_H(TCFlurryAdDelegates_711)

+ (void) registerBannerListener: (id<FlurryAdBannerDelegate>) listener;
+ (void) registerInterstitialListener: (id<FlurryAdInterstitialDelegate>) listener;

@property (nonatomic, retain) id<FlurryAdBannerDelegate> bannerListener;
@property (nonatomic, retain) id<FlurryAdInterstitialDelegate> interstitialListener;

@end

#endif
