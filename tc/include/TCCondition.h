//
//  TCCondition.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCRule.h"

@class TCDynamicResolver;
@class TCRule;

@interface TCCondition : NSObject<NSCopying>

- (BOOL) checkRules: (TCDynamicResolver *) solver;

@property (nonatomic, retain) NSMutableArray *rules;

@end
