//
//  ITCDynamicStore.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCShadowValue;

/**
* @protocol ITCDynamicStore
*
* The ITCDynamicStore protocol is a protocol to store dynamic variables.
*
* At its core, a dynamic store is a dictionary containing one value for each variable name
*
* The implementation is left to the implementing class but they need to handle three kinds
* of classes
*
* - simple variable value (NSString *)
* - shadow value TCShadowValue
* - mapList (NSArray *) of TCProduct
*
* @see TCShadowValue
* @see TCCheckCondition
* @see TCProduct
*/
@protocol ITCDynamicStore <NSObject>

/**
* Actually compute the value of one dynamic variable.
*
* @param variableName The dynamic variable variable
*
* @return The computed value of the dynamic variable
*/
- (NSString *) get: (NSString *) variableName;


/**
* Get the TCShadowValue the value of one dynamic variable.
*
* @param variableName The dynamic variable variable
*
* @return The computed value of the dynamic variable
*/
- (TCShadowValue *) getShadowValue: (NSString *) variableName;

/**
* Set a dynamic variable to a given string.
*
* It's the simplest way of putting a value (an NSString *) in a dynamic variable
*
* @param variableName The dynamic variable name (\#SCREEN\# for instance)
* @param variableValue The string associated to it ("home" for instance)
*
* @see set:withShadowValue:
*
*/
- (void) set: (NSString *) variableName withValue: (NSString *) variableValue;

/**
* Set a dynamic variable to a shadow value.
*
* It's the simplest way of putting a value (an NSString *) in a dynamic variable
*
* @param variableName The dynamic variable name (\#SCREEN\# for instance)
* @param variableValue The shadow value associated to it with zero or more TCFunction to apply
*
* @see set:withValue:
* @see TCShadowValue
* @see TCShadowValue
*
*/
- (void) set: (NSString *) variableName withShadowValue: (TCShadowValue *) variableValue;

/**
* Remove a dynamic variable from the TCDynamicStore
*
* @param variableName The dynamic variable variable
*
* @return As a convenience, the removed TCShadowValue of the dynamic variable before deleting it.
*/
- (TCShadowValue *) remove: (NSString *) variableName;

/**
* Clears the dynamic store.
*
* Has the effect of completely wiping out the storage of the dynamic store.
*/
- (void) clearStore;

/**
* Put an NSArray of TCProduct in the dynamic store.
*
*
* @param variableName The dynamic variable variable
* @param mapList An NSArray of TCProduct
* @see TCProduct
*
*/
- (void) set: (NSString *) variableName withMapList: (NSArray *) mapList;

/**
* Remove all keys (format \#KEY\#) from the dynStore.
* @todo document the mapList
*/
- (void) removeMapList: (NSString *) variableName;

/**
 * Create and hash if needed a map for the vendor asked.
 * Put into the right map
 * @param vendorID The vendor for the additional parameter.
 * @param parameter The key of the parameter.
 * @param value The value of the parameter.
 */
- (void) putForVendor: (NSString *) vendorID parameter: (NSString *) parameter withValue: (NSString *) value;

/**
 * Create and hash if needed a map for all vendors.
 * @param parameter The key of the parameter.
 * @param value The value of the parameter.
 */
- (void) putParametersForAllVendors: (NSString *) parameter withValue: (NSString *) value;

@end
