//
//  TCCustomURL_210.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCVendor.h"
#import "TCMacros.h"

@class TCTag;

@interface TCCustomURL_210 : TCVendor <ITCVendor>

SINGLETON_CLASS_H(TCCustomURL_210)

+ (NSString *) formattedCustomURLWithTag: (TCTag *) tag;
+ (NSString *) formatGetVariable: (NSString *) parameterName
                       withValue: (NSString *) parameterValue
              addAmpersandBefore: (bool) ampersandBefore;
@end
