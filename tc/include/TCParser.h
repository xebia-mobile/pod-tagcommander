//
//  TCParser.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TCContainer;
@class TCXMLReader;
@class TCTag;
@class TCCondition;
@class TCRule;

@interface TCParser : NSObject

- (id) initWithData: (NSData *) data;
- (TCContainer *) parse;
- (TCTag *) newTagWithDictionary: (NSDictionary *) tagDictionary;
- (TCCondition *) parseRules: (id) rulesDictionary;

+ (NSString *) removeQuotes: (NSString *) stringToUnquote;
+ (BOOL) isProvided: (NSString *) key;
+ (NSString *) convertStringToDynamicVariableFormat: (NSString *) stringToConvert;
+ (BOOL) isDynamicVariable: (NSString *) string;
+ (NSArray *) getDynamicVariables: (NSString *) string;
+ (NSArray *) matchRegexp: (NSString *) string forPattern: (NSString *) pattern;

@property(nonatomic, retain) NSData *data;
@property(nonatomic, retain) NSDictionary *XMLDictionary;
@property(nonatomic, retain, readonly) TCContainer *parsedContainer;
@property(nonatomic, assign) int siteID;
@property(nonatomic, assign) int containerID;


@end
