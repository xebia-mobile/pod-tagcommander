//
// Created by Jean-Julien ZEIL on 20/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef SMART_AD_SERVER_ENABLED

#import <Foundation/Foundation.h>
#import "ITCAdOperation.h"
#import "TCAdOperation.h"
#import "SASAdViewDelegate.h"

@class UIView;
@class TCHit;

static NSString *const kTCPageID = @"SAS_PageID";
static NSString *const kTCFormatID = @"SAS_FormatID";
static NSString *const kTCAdFormat = @"SAS_AdFormat";

static NSString *const kTCSASInterstitial = @"AdFormatInterstitial";
static NSString *const kTCSASBanner = @"AdFormatBanner";

@interface TCOperationSmartAdServer_441 : TCAdOperation<ITCAdOperation, SASAdViewDelegate>

- (id) initWithTag: (TCTag *) tag View: (UIView *) view;

@property (nonatomic, retain) NSString *formatID;
@property (nonatomic, retain) NSString *adFormat;
@property (nonatomic, retain) NSString *forceFullScreen;

@end

#endif