//
// Created by Jean-Julien ZEIL on 17/03/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCHit.h"

@class TCVendor;

@interface TCVendorOperation : NSOperation

- (id) initWithTCHit: (TCHit *) hit andVendor: (TCVendor *) vendor;

@property (nonatomic, retain) TCHit *hit;
@property (nonatomic, retain) TCVendor *vendor;

@end