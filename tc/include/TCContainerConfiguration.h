//
// Created by Damien Terrier on 11/03/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCContainerConfiguration : NSObject

- (id) initWithSiteID: (int) siteID andContainerId: (int) containerID;

@property (nonatomic, assign) int siteID;
@property (nonatomic, assign) int containerID;
@property(nonatomic, assign, readonly) NSString *downloadURL;
@property (nonatomic, assign) BOOL useCDN;
@end