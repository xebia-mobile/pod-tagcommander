//
//  TCWaitingCondition.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 13/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCWaitingCondition : NSObject<NSCoding>

- (id)initConditionWithName: (NSString *)name;

@property (nonatomic, retain) NSString *conditionName;

@end
