//
// Created by Jean-Julien ZEIL on 09/01/15.
// Copyright (c) 2015 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef FLURRY_ADSERVING_ENABLED

#import <Foundation/Foundation.h>
#import "TCAdOperation.h"
#import "ITCAdOperation.h"

@class UIView;
@class TCTag;

static NSString *const kTCBannerSizeBottom = @"BANNER_BOTTOM";
static NSString *const kTCBannerSizeTop = @"BANNER_TOP";
static NSString *const kTCBannerSizeFullScreen = @"FULLSCREEN";
static NSString *const kTCAdSpace = @"adSpace";
static NSString *const kTCBannerSize = @"bannerType";
static NSString *const kTCAutoDisplay = @"autoDisplay";

@interface TCOperationFlurry_711 : TCAdOperation<ITCAdOperation, FlurryAdBannerDelegate, FlurryAdInterstitialDelegate>

- (id) initWithTag: (TCTag *) tag View: (UIView *) view;

@property (nonatomic, retain) UIView *view;
@property (nonatomic, retain) FlurryAdBanner *banner;
@property (nonatomic, retain) FlurryAdInterstitial *interstitial;

@property (nonatomic, assign) BOOL autoDisplay;

@end

#endif
