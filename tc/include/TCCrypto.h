//
// Created by Jean-Julien ZEIL on 27/10/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TCCrypto : NSObject

+ (NSString *) encryptRSA: (NSString *) plainTextString key: (NSString *) key;
+ (Boolean) setPublicKey: (NSString *) pemPublicKeyString tag: (NSData *) tag;

@end