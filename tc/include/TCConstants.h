//
//  TCConstants.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#ifndef TagCommander_TCConstants_h
#define TagCommander_TCConstants_h

#import <Foundation/Foundation.h>

#define TC_UNIT_TESTS
#define TC_FUNCTIONAL_TESTS
#define TC_APPLICATION_TESTS

#pragma mark generated constants

#pragma mark - misc
extern NSString *const kTCConfigurationLabel;

extern NSString *const kTCFileOfflineHits;
extern NSString *const kTCFileOfflineVars;
extern NSString *const kTCLogger_LogFileName;
extern NSString *const kTCFilePrivateDocuments;

extern NSString *const kTCRecetteURL;

extern NSString *const kTCArrayFormat;

extern unsigned long long const kTCVisitDurationDefaultValue;

extern NSString *const kTCDecimalSeparator;

/** @section generalconstants General Constants */
extern NSString *const kTCManufacturerName;

// Special values created for ComScore
extern NSString *const kTCTimeSinceLastMeasurement;
extern NSString *const kTCSessionStartTimestamp;
extern NSString *const kTCLastSessionStartTimestamp;
extern NSString *const kTCExitTimestamp;
extern NSString *const kTCUsageDuration;
extern NSString *const kTCLastForegroundTime;
extern NSString *const kTCLastBackgroundTime;

#pragma mark Container Notifications
extern NSString *const kTCNotificationContainerFound;
extern NSString *const kTCNotificationContainerChanged;

#pragma mark Vendors Notifications
extern NSString *const kTCNotificationVendorCreated;
extern NSString *const kTCNotificationVendorError;
extern NSString *const kTCNotificationVendorFound;
extern NSString *const kTCNotificationVendorInit;
extern NSString *const kTCNotificationVendorInitialised;
extern NSString *const kTCNotificationVendorConfiguration;

#pragma mark HTTP Hit Notifications
extern NSString *const kTCNotificationHitSent;
extern NSString *const kTCNotificationHitExecute;
extern NSString *const kTCNotificationHitError;
extern NSString *const kTCNotificationAdExecute;
extern NSString *const kTCNotificationRemoveAd;

#pragma mark HTTP Request Notifications
extern NSString *const kTCNotificationHTTPRequest;
extern NSString *const kTCNotificationHTTPRequestSent;
extern NSString *const kTCNotificationHTTPRequestError;
extern NSString *const kTCNotificationHTTPResponse;

#pragma mark Configuration Request Notifications
extern NSString *const kTCNotificationConfigurationRequest;
extern NSString *const kTCNotificationConfigurationError;
extern NSString *const kTCNotificationConfigurationResponse;

#pragma mark Internet Notifications
extern NSString *const kTCNotificationInternetUp;
extern NSString *const kTCNotificationInternetDown;
extern NSString *const kTCNotificationInternetChanged;

#pragma mark Location Notifications
extern NSString *const kTCNotificationLocationAvailable;
extern NSString *const kTCNotificationLocationUnavailable;

#pragma mark Orientation Notifications
extern NSString *const kTCNotificationOrientationPortrait;
extern NSString *const kTCNotificationOrientationLandscape;
extern NSString *const kTCNotificationOrientationChanged;

#pragma mark Battery Notifications
extern NSString *const kTCNotificationLowBattery;
extern NSString *const kTCNotificationBatteryAvailable;

#pragma mark Background / Foreground notifications
extern NSString *const kTCNotification_OnBackground;
extern NSString *const kTCNotification_OnForeground;

#pragma mark Misc Notifications
extern NSString *const kTCNotificationAbstractMethodNotImplemented;
extern NSString *const kTCNotificationUnknown;

#pragma mark - Waiting Conditions
extern NSString *const kTCWaitingConditionInternetUp;
extern NSString *const kTCWaitingConditionConnexionType;
extern NSString *const kTCWaitingConditionGPSLatitude;
extern NSString *const kTCWaitingConditionGPSLongitude;
extern NSString *const kTCWaitingConditionIP;

#pragma mark - TCConfiguration
extern NSString *const kTCConfigurationURLStringFormat;
extern NSString *const kTCConfigurationCDNURLStringFormat;
extern NSString *const kTCConfigurationTestURL;

#pragma mark - TCSampling
extern NSString *const kTCSamplingURLStringFormat;
extern const int kTCDefaultSamplingFrequency;

#pragma mark - network
extern NSTimeInterval const kTCNetworkDefaultTimeout;
extern NSURLRequestCachePolicy kTCNetworkDefaultRequestCachePolicy;

#pragma mark - notification userInfo
extern NSString *const kTCUserInfo_DataKey;
extern NSString *const kTCUserInfo_URLKey;
extern NSString *const kTCUserInfo_ResponseKey;
extern NSString *const kTCUserInfo_ErrorKey;
extern NSString *const kTCUserInfo_RequestType;
extern NSString *const kTCUserInfo_AdditionalParametersKey;
extern NSString *const kTCUserInfo_ContentKey;
extern NSString *const kTCUserInfo_RequestID;
extern NSString *const kTCUserInfo_ShouldBeLogged;
extern NSString *const kTCUserInfo_ViewKey;

#pragma mark - Message
extern NSString *const kTCErrorMessageInvalidURL;

#pragma mark - XML Constants
extern NSString *const kTCXMLConditionEqual;
extern NSString *const kTCXMLConditionNotEqual;
extern NSString *const kTCXMLConditionOr;
extern NSString *const kTCXMLConditionNand;
extern NSString *const kTCXMLConditionAndMulti;
extern NSString *const kTCXMLConditionOrMulti;
extern NSString *const kTCXMLConditionExists;
extern NSString *const kTCXMLConditionContains;
extern NSString *const kTCXMLConditionGreaterThan;
extern NSString *const kTCXMLConditionLessThan;
extern NSString *const kTCXMLConditionIsEmpty;
extern NSString *const kTCXMLConditionSampling;

#pragma mark - predefined dynamic variables
extern NSString *const kTCPredefinedVariable_IP;
extern NSString *const kTCPredefinedVariable_Empty;
extern NSString *const kTCPredefinedVariable_Random;

extern NSString *const kTCPredefinedVariable_Language;
extern NSString *const kTCPredefinedVariable_LanguageGA;
extern NSString *const kTCPredefinedVariable_LanguageCS;
extern NSString *const kTCPredefinedVariable_SystemName;
extern NSString *const kTCPredefinedVariable_SystemVersion;
extern NSString *const kTCPredefinedVariable_Model;
extern NSString *const kTCPredefinedVariable_ModelAndVersion;
extern NSString *const kTCPredefinedVariable_Connexion;
extern NSString *const kTCPredefinedVariable_Device;

extern NSString *const kTCPredefinedVariable_ScreenResolution;
extern NSString *const kTCPredefinedVariable_Charset;
extern NSString *const kTCPredefinedVariable_CurrencyCode;
extern NSString *const kTCPredefinedVariable_CurrencySymbol;
extern NSString *const kTCPredefinedVariable_ApplicationVersion;
extern NSString *const kTCPredefinedVariable_ApplicationPreviousVersion;
extern NSString *const kTCPredefinedVariable_ApplicationBuild;
extern NSString *const kTCPredefinedVariable_TagCommanderVersion;
extern NSString *const kTCPredefinedVariable_Manufacturer;
extern NSString *const kTCPredefinedVariable_UserAgent;

extern NSString *const kTCPredefinedVariable_JailBroken;
extern NSString *const kTCPredefinedVariable_ColdStarts;
extern NSString *const kTCPredefinedVariable_ForegroundTransitions;
extern NSString *const kTCPredefinedVariable_ForegroundTime;
extern NSString *const kTCPredefinedVariable_DeltaForegroundTime;
extern NSString *const kTCPredefinedVariable_BackgroundTime;
extern NSString *const kTCPredefinedVariable_DeltaBackgroundTime;
extern NSString *const kTCPredefinedVariable_BackgroundUxTime;
extern NSString *const kTCPredefinedVariable_DeltaBackgroundUxTime;

extern NSString *const kTCPredefinedVariable_CurrentCall;
extern NSString *const kTCPredefinedVariable_CurrentCallMs;
extern NSString *const kTCPredefinedVariable_LastCall;
extern NSString *const kTCPredefinedVariable_LastCallMs;
extern NSString *const kTCPredefinedVariable_LastSessionStartMs;
extern NSString *const kTCPredefinedVariable_LastSessionLastHit;
extern NSString *const kTCPredefinedVariable_LastSessionLastHitMs;

extern NSString *const kTCPredefinedVariable_Now;
extern NSString *const kTCPredefinedVariable_NowMs;

extern NSString *const kTCPredefinedVariable_UniqueID;
extern NSString *const kTCPredefinedVariable_IDFA;
extern NSString *const kTCPredefinedVariable_IDFV;

/** opposite of TC_LIMIT_USER_TRACKING_ENABLED by definition */
extern NSString *const kTCPredefinedVariable_isTrackingEnabled;
extern NSString *const kTCPredefinedVariable_LimitUserTrackingEnable;

extern NSString *const kTCPredefinedVariable_Longitude;
extern NSString *const kTCPredefinedVariable_Latitude;

extern NSString *const kTCPredefinedVariable_BundleID;
extern NSString *const kTCPredefinedVariable_ApplicationName;
extern NSString *const kTCPredefinedVariable_RuntimeName;

extern NSString *const kTCUserDefaultsKey_UniqueID;

extern NSString *const kTCPredefinedVariable_FirstVisit;
extern NSString *const kTCPredefinedVariable_FirstVisitMs;
extern NSString *const kTCPredefinedVariable_LastVisit;
extern NSString *const kTCPredefinedVariable_LastVisitMs;
extern NSString *const kTCPredefinedVariable_CurrentVisit;
extern NSString *const kTCPredefinedVariable_CurrentSession;
extern NSString *const kTCPredefinedVariable_CurrentVisitMs;
extern NSString *const kTCPredefinedVariable_CurrentSessionMs;
extern NSString *const kTCPredefinedVariable_SessionDuration;
extern NSString *const kTCPredefinedVariable_SessionDurationMs;
extern NSString *const kTCPredefinedVariable_CurVersionFirstVisitMs;

extern NSString *const kTCPredefinedVariable_NumberVisits;
extern NSString *const kTCPredefinedVariable_NumberSessions;

extern NSString *const kTCPredefinedVariable_IsNewSession;
extern NSString *const kTCPredefinedVariable_UserSessionDurationMs;
extern NSString *const kTCPredefinedVariable_UsageSessionDurationMs;
extern NSString *const kTCPredefinedVariable_AccumulatedBackgroundTime;
extern NSString *const kTCPredefinedVariable_TimeSinceLastExit;
extern NSString *const kTCPredefinedVariable_AccumulatedForegroundTimeWithoutMeasurement;
extern NSString *const kTCPredefinedVariable_LastForegroundTimeWithoutMeasurement;

extern NSString *const kTCPredefinedVariable_EmptyVariableRemoveEqual;

#pragma mark - Additional parameters names
extern NSString *const kTCAdditionalParameter_Keyword;
extern NSString *const kTCAdditionalParameter_Cookies;

#pragma mark - Timestamp formats
extern NSString *const kTCDateFirstVisitFormat;
extern NSString *const kTCDateLastVisitFormat;
extern NSString *const kTCDateCurrentVisitFormat;
extern NSString *const kTCNumberColdStarts;
extern NSString *const kTCNumberVisitsFormat;
extern NSString *const kTCLastPID;

#pragma mark - Battery level
extern const float kTCLowBatteryWarningLevel;
extern const float kTCBatteryAvailableWarningLevel;

#pragma mark - Regexps
extern NSString *const kTCRegexpDynamicVariable;

extern NSString *const kTCRegexpIP;
extern NSString *const kTCRegexpAppleLocale;
extern NSString *const kTCRegexpScreenResolution;
extern NSString *const kTCRegexpSoftwareVersion;
extern NSString *const kTCRegexpUUID;

extern NSString *const kTCPatternArrayArguments;
extern NSString *const kTCPatternArrayElementIndex;
extern NSString *const kTCPatternArrayKey;

#endif
