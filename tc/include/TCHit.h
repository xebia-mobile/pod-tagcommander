//
//  TCHit.h
//  TagCommander
//
//  Created by Jean-Julien ZEIL on 10/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TCTag.h"
#import "TCWaitingCondition.h"

@class TCTag;

typedef enum TCHitType
{
    TCHitType_Adserving,
    TCHitType_Analytics,
} TCHitType;

typedef enum TCHitEventType
{
    TCHitEventType_View,
    TCHitEventType_Click
} TCHitEventType;

@interface TCHit : NSObject<NSCoding, ITCDynamicStore>

- (id) initHitWithTag: (TCTag *)tag;
- (void) addWaitingCondition: (TCWaitingCondition *)condition;

- (BOOL) isWaitingForCondition: (NSString *)condition;
- (void) removeWaitingCondition: (NSString *)condition;

+ (BOOL (^)(NSDictionary *)) isValidAnalyticHitInUserInfo;

@property (nonatomic, assign) int ID;
@property (nonatomic, assign) int timestamp;
@property (nonatomic, copy)   NSString *libID;
@property (nonatomic, assign) TCHitType type;
@property (nonatomic, assign) TCHitEventType eventType;
@property (nonatomic, retain) TCTag *tag;
@property (nonatomic, retain) NSObject *extras;

@property(nonatomic, retain) TCDynamicStore *dynamicStore;

@property (nonatomic, retain) NSMutableArray *waitingConditions;

@end
