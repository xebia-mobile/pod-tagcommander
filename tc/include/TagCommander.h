//
//  TagCommander.h
//  TagCommander
//
//  Created by Damien TERRIER on 03/02/14.
//  Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "generated.h"

#import "TCMacros.h"

#import "TCDynamicStore.h"
#import "TCEventListener.h"
#import "TCEventSender.h"
#import "TCProduct.h"
#import "TCAppVars.h"
#import "TCState.h"
#import "TCLogger.h"
#import "TCPredefinedVariables.h"
#import "TCAdditionalParameters.h"
#import "TCUser.h"
#import "TCVendorConstants.h"
#import "TCConstants.h"
#import "TCLog.h"


#ifdef COMSCORE_ENABLED
#import "TCComScore_21409.h"
#endif

#ifdef FLURRY_ENABLED
#import "TCFlurry_711.h"
#import "TCFlurry_711+FlurryCompatibility.h"
#endif

#ifdef GOOGLE_ANALYTICS_ENABLED
#import "TCGoogleAnalytics_5077.h"
#endif

#ifdef CRITEO_ENABLED
#import "TCCriteo_10.h"
#endif

@class TCAdServing;
@class TCContainer;
@class TCHit;
@class TCDynamicResolver;
@class TCTag;

@interface TagCommander : NSObject <ITCDynamicStore, ITCEventListenerDelegate, ITCEventSenderDelegate>

/**
* Designated initializer.
*
* @param siteID The TagCommander Side ID.
* @param containerID The TagCommander Container ID.
*/
- (id) initWithSiteID: (int) siteID andContainerID: (int) containerID;

/**
 * Executes an analytics hit
 *
 * @param appVars The AppVars needed to send the hits.
 *
 */
- (void) execute: (TCAppVars *) appVars;
- (void) processAppVars: (TCAppVars *) appVars;
- (TCHit *) createHitWithAppVars: (TCAppVars *) appVars tag: (TCTag *) tag;

- (void) saveAllHits;
- (void) restoreStockedHit;

#pragma mark - public properties
@property (nonatomic, assign) int containerID;
@property (nonatomic, assign) int siteID;
@property (nonatomic, retain) TCContainer *container;

@property (nonatomic, retain) TCDynamicStore *dynamicStore;
@property (nonatomic, retain) TCEventSender *senderDelegate;
@property (nonatomic, retain) TCEventListener *listenerDelegate;

#pragma mark - deprecated
// Deprecated

/** @deprecated **/
- (void) TCExecute: (NSMutableDictionary *) my_parameters __attribute__((deprecated));
/** @deprecated **/
- (void) TCInit: (int) id_site andContainer: (int) id_container andZone: (UIView *) view andController: (NSObject *) controller andRefresh: (int) refresh __attribute__((deprecated));
/** @deprecated **/
- (void) TCStart __attribute__((deprecated));
/** @deprecated **/
- (void) TCStart: (NSMutableDictionary *) my_parameters __attribute__((deprecated));
/** @deprecated **/
- (void) TCExecute __attribute__((deprecated));
/** @deprecated **/
- (void) TCStop: (NSMutableDictionary *) my_parameters __attribute__((deprecated));
/** @deprecated **/
- (void) TCStop __attribute__((deprecated));
/**
* @deprecated
*
* @example
* //use instead
* Use [[TCState sharedInstance] setForceReload: YES]; instead
*/
- (void) forceReload __attribute__((deprecated));
/** @deprecated
*
* @example
* //use instead
* [[TCState sharedInstance] setDebugForLevel: TCLogLevel_Debug andOutput: TCLogOutput_ConsoleFile];
* [[TCLogger sharedInstance] setDebugVerboseNotificationLog: YES];
*
*/
- (void) activeDebug __attribute__((deprecated));
// !Deprecated
@end

@compatibility_alias tagcommander TagCommander;
