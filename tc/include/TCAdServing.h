//
// Created by Damien TERRIER on 18/09/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "ITCEventSenderDelegate.h"

@class TagCommander;
@class UIView;
@class TCAppVars;
@class TCEventSender;
@class TCAdOperation;

@interface TCAdServing : NSObject<ITCEventSenderDelegate>

SINGLETON_CLASS_H(TCAdServing)

#pragma mark - ITCAdServing
- (void) setRootViewController: (UIViewController *) uiRootViewController;
- (void) adExecute: (TagCommander *) TC withAppVars: (TCAppVars *) appVars view: (UIView *) adView;
- (void) removeAd: (NSString *) adID;
- (BOOL) checkRootControllerAvailable;

@property (nonatomic, assign) UIViewController *rootViewController;

@property (nonatomic, assign) int totalAdsSent;
@property (nonatomic, retain) TCEventSender *senderDelegate;

@end
