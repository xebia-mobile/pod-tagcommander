//
// Created by Damien TERRIER on 18/09/14.
// Copyright (c) 2014 TagCommander. All rights reserved.
//

#import "generated.h"

#ifdef FLURRY_620_ENABLED

#import <Foundation/Foundation.h>
#import "TCMacros.h"
#import "TCVendor.h"

#import "Flurry.h"
#import "FlurryAds.h"
#import "TCAdVendor.h"
#import "ITCAdVendor.h"

@class TCAdOperation;
@class TCTag;
@class FlurryAdTargeting;

@interface TCFlurry_620 : TCAdVendor<ITCVendor, ITCAdVendor>

SINGLETON_CLASS_H(TCFlurry_620)

- (TCAdOperation *) createOperationForTag: (TCTag *) tag view: (UIView *) adView;
- (FlurryAdTargeting *) userAdapter;

@property (retain, nonatomic) UIViewController *rootViewController;
@property (retain, nonatomic) UIView *adView;

@end

#endif